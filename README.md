# Certmanager
Certmanager is a user friendly and interactive script to maintain TLS-Certificates from Let's Encrypt.

This script uses [acme-tiny](https://github.com/diafygi/acme-tiny) to obtain certificates from Let's Encrypt.

## Instructions to install and maintain this application.
### This code snippet is used to install certmanager.
```
#!/bin/bash
mkdir "/opt/certmanager"

groupadd -r certmanager
useradd -r -g certmanager --no-create-home -s /bin/bash certmanager
chown certmanager:certmanager "/opt/certmanager"
su certmanager

git clone "https://codeberg.org/Masgalor/Certmanager.git" "/opt/certmanager"
git -C "/opt/certmanager" submodule init
git -C "/opt/certmanager" submodule update
```
### This code snippet is used to update certmanager.
```
#!/bin/bash
git -C "/opt/certmanager" pull
git -C "/opt/certmanager" submodule update
```
### This code snippet can be used to set the recommended permissions for all executables.
```
#!/bin/bash
chmod 550 "/opt/certmanager/certmanager.sh"
chmod 550 "/opt/certmanager/include/acme-tiny/acme_tiny.py"
```

## Supported functions
* create:
```
Creates a new configuration and corresponding certificates, existing files will be overwritten.
All generated certificates will be signed automatically.
```
* renew:
```
Generates and signs new public certificates for ALL existing configurations.
Existing configurations and private certificates will remain untouched.
Can only be used if a valid configuration and private certificate were created before.
```
* complete-renew:
```
Generates and signs new public AND private certificates for ALL existing configurations.
Existing configurations will remain untouched.
Can only be used if a valid configuration was created before.
```
* manual-renew:
```
Generates and signs new public AND private certificates for ONE selected configuration.
Existing configurations will remain untouched.
Can only be used if a valid configuration was created before.
```
* delete:
```
Deletes the configuration and private/public certificates of a given name.
```
* sync:
```
Synchronizes your private and public Certificates with another Server.
CSR files may be transmitted as well.
```
